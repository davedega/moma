package com.dega.moma.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dega.moma.BaseActionBarActivity;
import com.dega.moma.R;
import com.dega.moma.views.ITutorialViewModel;
import com.dega.moma.views.ITutorialViewModelListener;

public class TutorialController extends BaseActionBarActivity implements
		ITutorialViewModelListener {

	ITutorialViewModel viewModel;

	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, TutorialController.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(intent);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		viewModel = (ITutorialViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_tutorial);
		viewModel.setListener(this);
	}

	@Override
	public void onCloseT() {
		finish();
	}

}

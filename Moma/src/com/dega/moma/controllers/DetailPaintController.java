package com.dega.moma.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.dega.moma.R;
import com.dega.moma.R.id;
import com.dega.moma.R.layout;
import com.dega.moma.R.menu;
import com.dega.moma.views.IDetailPaintViewModel;
import com.dega.moma.views.IDetailPaintViewModelListener;

public class DetailPaintController extends ActionBarActivity implements
		IDetailPaintViewModelListener {

	IDetailPaintViewModel viewMdolel;
	
	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, DetailPaintController.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_detail_paint);
		viewMdolel = (IDetailPaintViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_detail);
		viewMdolel.setListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail_paint_controller, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			break;
		case android.R.id.home:
			finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackT() {
		// TODO Auto-generated method stub

	}

}

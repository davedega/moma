package com.dega.moma.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.dega.moma.BaseActionBarActivity;
import com.dega.moma.MOMA;
import com.dega.moma.R;
import com.dega.moma.R.id;
import com.dega.moma.R.layout;
import com.dega.moma.R.menu;
import com.dega.moma.views.IGauguinViewModel;
import com.dega.moma.views.IGauguinViewModelListener;

public class GauguinController extends BaseActionBarActivity implements
		IGauguinViewModelListener {
	IGauguinViewModel viewModel;

	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, GauguinController.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		ctx.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.activity_gauguin);
		viewModel = (IGauguinViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_gaugin);
		viewModel.setListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gauguin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			break;
		case android.R.id.home:
			finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDetailT() {
		DetailPaintController.startActivity(getApplicationContext());

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		MOMA.getInstance().setCurrentBeacon("free");
	}
}

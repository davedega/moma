package com.dega.moma.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dega.moma.R;
import com.dega.moma.controllers.DiegoController;
import com.dega.moma.controllers.GauguinController;

public class ExhibitionsView extends Fragment {

	String[] exhibitions;

	LinearLayout mView;
	ListView lv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_exhibitions,
				container, false);
		lv = (ListView) mView.findViewById(R.id.exhibitions_list);
		exhibitions = getResources().getStringArray(R.array.fi);

		ExhibitionsAdapter adapter = new ExhibitionsAdapter(getActivity(),
				R.layout.exhibitions_item_listview, exhibitions);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Log.i("", "" + position);
				if (position == 0)
					GauguinController.startActivity(getActivity());
				else if (position == 1)
					DiegoController.startActivity(getActivity());

			}
		});
		return mView;
	}

	public class ExhibitionsAdapter extends ArrayAdapter<String> {
		TextView name;
		int view;
		String[] exs;

		public ExhibitionsAdapter(Context context, int resource,
				String[] exhibitions) {
			super(context, resource, exhibitions);

			view = resource;
			exs = exhibitions;

		}

		@Override
		public int getCount() {
			return super.getCount();
		}

		@Override
		public String getItem(int position) {
			return super.getItem(position);
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(view, parent, false);
			}
			name = (TextView) row.findViewById(R.id.name_exhibition);

			name.setText(exs[position]);
			return row;
		}
	}
}

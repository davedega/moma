package com.dega.moma.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dega.moma.R;
import com.dega.moma.views.GauguinView.PaintsAdapter;

public class DiegoView extends Fragment implements IGauguinViewModel {
	LinearLayout mView;
	IGauguinViewModelListener mListener;
	ListView lv;
	PaintsAdapter adapter;
	String[] paints;
	int[] pics;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_gauguin,
				container, false);

		lv = (ListView) mView.findViewById(R.id.paints_lv);

		paints = getResources().getStringArray(R.array.diego_paints);
		pics = new int[] { R.drawable.diego_zero, R.drawable.diego_uno,
				R.drawable.diego_dos, R.drawable.diego_tres,
				R.drawable.diego_cuatro, R.drawable.diego_cinco,
				R.drawable.diego_zero };

		Log.i("", "paints: " + paints.length);
		Log.i("", "pics: " + pics.length);

		adapter = new PaintsAdapter(getActivity(),
				R.layout.paints_item_listview, paints, pics);

		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mListener.onDetailT();
				
			}
		});
		return mView;
	}

	@Override
	public void setListener(IGauguinViewModelListener listener) {
		mListener = listener;
	}

	public class PaintsAdapter extends ArrayAdapter<String> {
		TextView name;
		ImageView image;
		int view;
		String[] exs;
		int[] paints;

		public PaintsAdapter(Context context, int resource,
				String[] exhibitions, int[] imgs) {
			super(context, resource, exhibitions);

			view = resource;
			exs = exhibitions;
			paints = imgs;
		}

		@Override
		public int getCount() {
			return super.getCount();
		}

		@Override
		public String getItem(int position) {
			return super.getItem(position);
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(view, parent, false);
			}
			name = (TextView) row.findViewById(R.id.name_paint);
			image = (ImageView) row.findViewById(R.id.image_paint);
			name.setText(exs[position]);
			image.setImageResource(paints[position]);
			return row;
		}
	}

}

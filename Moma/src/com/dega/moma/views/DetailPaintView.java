package com.dega.moma.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dega.moma.R;

public class DetailPaintView extends Fragment implements IDetailPaintViewModel {
	IDetailPaintViewModelListener mListener;
	LinearLayout mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_detail_paint,
				container, false);
		return mView;

	}

	@Override
	public void setListener(IDetailPaintViewModelListener listener) {
		mListener = listener;
	}

}
